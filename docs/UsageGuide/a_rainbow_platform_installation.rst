###############################################################
Setup
###############################################################

Here you can find instructions for the installation of Rainbow Platform. 
This page references the repository under https://gitlab.com/rainbow-project1/rainbow-installation 


==========
Requirements
==========

For the advanced platform package, that includes everything, the minimum requirements are:

+-------------------+------------+-------------+
| **Component**     | **#vCPUs** | **RAM(GB)** |
+===================+============+=============+
| k8s master        |      4     |      10     |
+-------------------+------------+-------------+
| k8s worker server |      2     |      8      |
+-------------------+------------+-------------+
| k8s rpi worker    |      2     |      8      |
+-------------------+------------+-------------+
| k8s jetson worker |      2     |      8      |
+-------------------+------------+-------------+
| Rainbow Dashboard |      2     |      8      |
+-------------------+------------+-------------+

For the core package platform package, that does not include complex analytics, RAM can be dropped to 4GB.

+-------------------+------------+-------------+
| **Component**     | **#vCPUs** | **RAM(GB)** |
+===================+============+=============+
| k8s master        |      4     |      10     |
+-------------------+------------+-------------+
| k8s worker server |      2     |      4      |
+-------------------+------------+-------------+
| k8s rpi worker    |      2     |      4      |
+-------------------+------------+-------------+
| k8s jetson worker |      2     |      4      |
+-------------------+------------+-------------+
| Rainbow Dashboard |      2     |      4      |
+-------------------+------------+-------------+


========
Master Setup
========

For master setup you will need to follow the steps below: 

1. Clone the `repository <https://gitlab.com/rainbow-project1/rainbow-installation>`_.
2. Go to ***installation-scripts*** directory and give execution rights to all scripts.
3. Update the following lines of the ***rainbow-v2-master.sh*** script with the appropriate values.

.. code-block:: console

	docker_server="<DOCKER_SERVER>"
	docker_username="<DOCKER_USERNAME>"
	docker_password="<DOCKER_PASSWORD>"
	docker_email="<DOCKER_EMAIL>"


4. Execute ***rainbow-v2-master.sh*** script.
5. After script execution it prints out the **kubernetes** and **cjdns** credentials. Those credentials are necessary for the next steps, workers setup and dashboard setup.

========
Workers Setup
========

For master setup you will need to follow the steps below: 

1. Clone the `repository <https://gitlab.com/rainbow-project1/rainbow-installation>`_.
2. Go to ***installation-scripts*** directory and give execution rights to all scripts.
3. Update the following lines of the ***rainbow-v2-worker.sh*** script with the appropriate values. *serverIP* is the master's ip, the other values are the **cjdns** credentials from the **Master** setup step.

.. code-block:: console

	serverIP="<SERVER_IP>"
	port="<SERVER_PORT>"
	password="<PASSWORD>"
	publicKey="<PUBLICKEY>"
	serverHostname="<HOSTNAME>"
	serverIPv6="<IPv6>"


4. Execute ***rainbow-v2-worker.sh*** script.


========
Special Case
========

If there are NVIDIA Xavier devices on the cluster go to ***xavier-device*** directory and follow the instructions

