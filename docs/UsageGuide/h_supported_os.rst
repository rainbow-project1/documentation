###############################################################
Supported OS
###############################################################

**Servers:** 

We support every distrubution system, but currently the scripts work only for debian and ubuntu. if there is a hard limitation please contact in order to extend the scripts for other distributions

**RPI:**

We support only 64bit ubuntu based distributions due to limitation of k8s working with calico as network plugin

**Nvidia jetson/xavier/etc:** 

Kernerl recompilation is essential to enable flags.

**CJDNS enablement for Xavier:**

Instructions can be found under https://gitlab.com/rainbow-project1/rainbow-installation/-/tree/main#cjdns-enablement-for-xavier
