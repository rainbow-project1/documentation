###############################################################
Components
###############################################################

In Rainbow Platform each service is represented by a Component which is instantiated as a docker process in a unique Virtual Machine. 
Then applications are formed by connecting individuals components (VMs) together. 
 

========
View Available Components
========

By selecting Components from the left sidebar menu, the user is redirected to the list of the available components.

.. image:: assets/components.png


========
Create a Component
========

The user can create a new component by clicking on the button <Create new> at the top right corner of the page.
Then there is a tabbed form where the configuration of the component takes place.

- General Settings can be configured as the component name, machine architecture, and elasticity controller scale function. 

.. image:: assets/create_components_1.png


- The "blueprint" of the component, which is the docker image and the registry of it, are set in the Distribution Parameters tab.
	  - Custom Docker registries are supported.

.. image:: assets/create_components_2.png

- The communication protocol is setting by exposing or requiring interfaces, which is the network port that the Component exposes or requires to communicate.
   - A user can select one of the existing interfaces, like a TCP access through port 80, or define a new interface.
   - For the definition of the required interface, an existing exposed interface of another component has to be selected.

.. image:: assets/create_components_3.png

- Advanced Options can be set such as run the component image in host mode, priviliged mode, add System Capablities, assign hostname 

.. image:: assets/create_components_4.png


The user can also:

* Specify resources limit for the Component (VCPUs, RAM, Storage, Hypervisor Type)
* Set Enviromental Variables
* Define Health Check Commands and Entrypoint Commands
* Map Volumes
* Add Plugins that are available in the Plugins Section
* Add labels to the component 

========
View/Edit a Component
========

The user can view and edit the parameters of an existing component by clicking Edit button in the right corner of the item list.


.. image:: assets/edit_component.png

