###############################################################
Application Instances
###############################################################

After an application has been configured, an instance of it can be deployed to the cloud.

=============================
Create an Application Instance
=============================

- To create a new Application Instance, the user has to select <Create Instance> at the top right side of application list item.

.. image:: assets/create_app_instance_1.png


Prior to the deployment some settings can be configured: 

- Genereal settings concerning the deployment infrastructure.

.. image:: assets/create_app_instance_3.png

- Security mechanisms such as Intrusion Prevention Mechanism (IPS), Security Operations Center (SOC), Intrusion Detection Mechanism (IDS)

.. image:: assets/create_app_instance_4.png

- Interface parameters

.. image:: assets/create_app_instance_5.png 

- Environmental variables of the components.

.. image:: assets/create_app_instance_6.png 

- Advanced options such as network mode host or priviliged mode

.. image:: assets/create_app_instance_7.png


The user can also:

* Specify the minimum and maximum amount of workers per node that control the scalability profile of the application.
* Set Health Check Commands and Entrypoint Execution Commands
* Map Volumes
* Mange Component Plugins 
* Manage components' labels 


=============================
Deploy an Application Instance
=============================

- By pressing "Proceed" the deployment starts.

.. image:: assets/deploy_app_1.png

- At the Instances View, the user can see the list of deployed instances, identifiers and their status.

.. image:: assets/deploy_app_2.png
 
- Deployment procedure needs few minutes to finish. The user is constantly informed by viewing the logs aggregated from all the nodes of the application.
   - The total deployment time depends on the cloud infrastructure selected, as the spawning of new VMs might take more time in some IaaS.
   - Total time is also affected by the network delays between the cloud infrastructure and the docker registry that is used to fetch the components container image.

.. image:: assets/deploy_app_3.png

- When deployment finishes all nodes turn green
   - On the instance list the application is shown as "DEPLOYED".

