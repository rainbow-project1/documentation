###############################################################
Getting started with Rainbow Dashboard
###############################################################

Here you can find instructions for the basic functionalities of Rainbow Platform.

========
Login/Logout
========


Login
------------


- Provide your login credentials and click the <SIGN IN> button.

.. image:: assets/login.png

- Upon successful authentication the following screen will be presented. 

.. image:: assets/dashboard.png


Logout
----------

- In order to perform logout  click the <Log-out > Button.

.. image:: assets/logout.png

- Upon successful logout the following screen will be presented.

.. image:: assets/login.png


========
Dashboard Main View
========

The main dashboard: 

- Contains useful information on the cloud resources usage (CPU, Memory, Instances) 
- Provides health tracking logs on the running instances 
- Lists elasticity and security policies 
- Presents a total overview of the existing components, applications, vCPUS and Ram available.  

.. image:: assets/dashboard.png




==========
Platform Usage Video
==========

This screencast can also help you get started:

.. raw:: html


