###############################################################
Applications
###############################################################

- After the needed components have been defined, the user can proceed with the definition of the application.

- The application will be created through the help of a visual graph editor and then will be available for deployment.

==================
View Available Applications
==================


By selecting Applications from the left sidebar menu, the user is redirected to the list of the available applications.

.. image:: assets/applications.png

========
Create An Application
========

- To create a new Application, the user has to select <Create new> at the top right corner of the page.
- The user is redirected to the visual edito, where the application components are presented as the nodes of a graph. 
- The connection between the nodes is describing the interfaces between the components - services.

.. image:: assets/create_app_1.png


- Through the left side panel, the components can be retrieved and added to the editor.

.. image:: assets/create_app_2.png

- By selecting the required interface and dragging it to another node, the connection between the interfaces of the components can be done.

.. image:: assets/create_app_3.png

- This procedure is followed until all required interfaces have been connected in order to save a valid application graph.

.. image:: assets/create_app_4.png

