###############################################################
Dashboard Setup
###############################################################

Here you can find instructions for the Dashboard Setup, which is the last step of the whole setup procedure.
This page references the repository under https://gitlab.com/rainbow-project1/rainbow-installation.
 
For installation a token is required, which can be requested at ktheodosiou@ubitech.eu and avasileiou@ubitech.eu


1. Clone the `repository <https://gitlab.com/rainbow-project1/rainbow-installation>`_.
2. Go to ***dashboard*** directory and give execution rights to all scripts.
3. Update the following lines of the ***rainbow-dashboard.sh*** script with the appropriate values. *serverIP* is the master's ip, the other values are the **cjdns** credentials from the **Rainbow Setup - Master** setup step.


.. code-block:: console

	serverIP="<SERVER_IP>"
	port="<SERVER_PORT>"
	password="<PASSWORD>"
	publicKey="<PUBLICKEY>"
	serverHostname="<HOSTNAME>"
	serverIPv6="<IPv6>"


4. Execute ***rainbow-dashboard.sh***
5. Update the following lines of the ***.env*** file with the appropriate values. Server here is the local machine where the setup takes place.

.. code-block:: console

	SERVER_IP=<SERVER_IP>
	SERVER_BASE_PATH=<VOLUME_DATA_PATH>


6. Execute the following command

.. code-block:: console

	docker-compose up -d


