###############################################################
Managing and Using Volumes
###############################################################



=============================
Rainbow Volumes and Workspace
=============================

On you workspace, create a proper path for your containers

.. image:: assets/volumes2.png


On you workspace and on the proper path,  upload yours files

.. image:: assets/volumes3.png


=============================
Rainbow Volumes and Application
=============================

On component registration  or edit, you can add your container volume paths

.. image:: assets/volumes1.png


At last, on create instance and on component Parameterization, set the host paths using the yours workspace path (use only directory paths  and NO file paths)


.. image:: assets/volumes4.png
