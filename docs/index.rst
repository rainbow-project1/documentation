.. UCO documentation master file, created by
   sphinx-quickstart on Tue Aug 26 14:19:49 2014.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to the online documentation of Rainbow Project and Rainbow Platform!
=======================================================


Introduction to Cloud Computing, Containers, Microservices and DevOps
------------

*Cloud Computing has reached virtually all areas of society and its impact on service development, production, provision and consumption is manifold and far-reaching. It lowers innovation barriers and thereby impacts industry, small and large businesses, governments and society, and offers significant benefits for everyone.*

*According to* `Gartner Inc <https://www.gartner.com/newsroom/id/3871416>`_
*, while cloud computing has been an application deployment and infrastructure management paradigm for many years now, the cloud market is still expanding, reaching an impressive $200bn milestone projection for 2016 with an increasing growth rate of 16%.
In this digital economy, Small and Medium Enterprises (SMEs) and today’s Startups are migrating core services and products of their business to the cloud. Recent studies shows that in 2015 more than 37% of SMEs have embraced the cloud to run parts of their business, while projections show that by 2020 this number will grow and reach 80%. However, properly preparing for tomorrow’s cloud challenges is crucial if one wants to unleash the full potential of the technology.*

*Below is a set of resources in the form of dissemination and scientific articles, implementation examples, blog entries, videos, tutorials and courses and at different levels of difficulty.
The purpose of collecting this information is to give potential users of the Rainbow platform quick access to useful and high quality resources on different related topics, namely, general information on Cloud Computing, information on the challenges one must face when deciding to adopt this technology and, finally, aspects related to the agile processes of software development for Cloud Computing.*

*Moreover, you can read some useful information about analytic services, decision making, auto-scaling and monitoring using the RAINBOW platform.*



What is Rainbow!
------------

A framework that allows the design and deployment of secure and elastic by design cloud applications and services.



Rainbow Technology Stack
------------
Developed on top of popular and open-source frameworks including Kubernetes, Docker, CoreOS to support multi-cloud application runtime management



Why Rainbow?
------------
- All Rainbow apps are packaged and enabled by Docker Runtime Engine to create and isolate the containerized execution environment. 
  * Docker Runtime Engine and Docker Compose are tools sufficient for small deployments, but limited to a single host.

- Kubernetes can support the orchestration of large-scale distributed containerized deployments spanning across multiple hosts.
  * However Kubernetes has limitations in regard on the provisioning and deprovisioning of infrastructure resources and the auto-scaling. 
  * Also Kubernetes cannot support cross-cloud deployments.



- Underlying containerized environment based on CoreOS which enables fast boot times and secure-out-of-the Docker runtime.
  * Enhanced by security service to filter network traffic and apply privacy preserving ruling.

- Rainbow Smart Orchestrator is suitable for Highly Available (HA) host management.
  * Taps into auto-scaling offered by cloud offerings to estimate and assess app elasticity behavior and scaling effects. 
  * Low-cost and self-adaptive monitoring to reduce network traffic propagation.

- Rainbow Smart Orchestrator enables deployments across multiple cloud sites.
  * Cross-cloud network overlay is provided.

- Compatibility with Docker Compose is preserved as an extension of Docker Compose is used to describe, configure and deploy multi-container applications using YAML syntax.









Online Documentation Contents
=====================================================================

.. toctree::
   :maxdepth: 2
   :caption: Basic Usage Guide
   
   UsageGuide/h_supported_os
   UsageGuide/a_rainbow_platform_installation
   UsageGuide/analytic_stack/analytic_stack_setup
   UsageGuide/i_dashboard_setup
   UsageGuide/b_getting_started_with_rainbow
   UsageGuide/c_components
   UsageGuide/d_application
   UsageGuide/e_application_instances
   UsageGuide/f_monitoring_and_scaling
   UsageGuide/g_cloud_resources



.. toctree::
  :maxdepth: 2
  :caption: Support
  
  support
